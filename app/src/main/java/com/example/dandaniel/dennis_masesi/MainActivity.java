package com.example.dandaniel.dennis_masesi;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText millimetres;
    private EditText Inches;
    private Button Convert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        millimetres= findViewById(R.id.et_mil);
        Inches=findViewById(R.id.et_displayInch);
        Convert=findViewById(R.id.btn_convert);

        Convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Inches.getText().toString().isEmpty()){
                    convert_mm_to_Inches();
                }
            }
        });
    }
    @SuppressLint("SetTextI18n")
    public void convert_mm_to_Inches(){
        double val=Double.parseDouble(millimetres.getText().toString());

        Inches.setText(Double.toString(val/25.4));

    }
    public void exit(View v){
        finish();
    }

}
